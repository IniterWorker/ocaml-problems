(* 4. Find the number of elements of a list. (easy) *)

let length l =
  let rec aux n = function
    | [] -> n
    | _ :: tl -> aux (n + 1) tl
  in
  aux 0 l
