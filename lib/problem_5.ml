(* 5. Reverse a list. (easy) *)

let rev l =
  let rec aux acc = function
    | [] -> acc
    | hd :: tl -> aux (hd :: acc) tl
  in
  aux [] l
