let rec last_two = function
  | [] | [_] -> None
  | [a;b] -> Some (a, b)
  | _ :: tl -> last_two tl
