(* Flatten a nested list structure. (medium) *)

(* use rev list orderer in problem_5 *)
open Problem_5 

type 'a node =
    | One of 'a 
    | Many of 'a node list

let flatten l =
  let rec aux acc = function
    | [] -> acc
    | One x::tl -> aux (x :: acc) tl
    | Many x::tl -> aux (aux acc x) tl
  in
  rev (aux [] l)
