(* 6. Find out whether a list is a palindrome. (easy) *)

open Problem_5

let is_palindrome x = x = rev x
