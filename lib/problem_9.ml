(* Pack consecutive duplicates of list elements into sublists. (medium) *)

let pack l =
  let rec aux acc current = function
    | [] -> []
    | [x] -> (x :: current) :: acc
    | a :: (b :: _ as tl) ->
       if a = b
       then aux acc (a :: current) tl
       else aux ((a :: current) :: acc) [] tl
  in
  List.rev (aux [] [] l)
    
