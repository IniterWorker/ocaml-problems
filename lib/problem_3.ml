(*  Find the k'th element of a list. (easy)  *)

(* minimum lines *)
let rec at n = function
  | [] -> raise Not_found
  | hd :: tl -> if n = 0 then hd else at (n - 1) tl

(* safe way *)
let at_safe n l =
  if n < 0 then invalid_arg "at_safe" else
    let rec aux n = function
      | [] -> raise Not_found
      | hd :: tl -> if n = 0 then hd else aux (n - 1) tl
    in
    aux n l
    
(* Option way *)
let at_safe_option n l =
  if n < 0 then invalid_arg "at_safe" else
    let rec aux n = function
      | [] -> None
      | hd :: tl -> if n = 0 then Some (hd) else aux (n - 1) tl
    in
    aux n l
      
