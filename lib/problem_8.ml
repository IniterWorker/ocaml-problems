(* 8. Eliminate consecutive duplicates of list elements. (medium) *)

let rec compress = function
  | lf :: (rf :: _ as tl ) -> if lf = rf then compress tl else lf :: compress tl 
  | smaller -> smaller
