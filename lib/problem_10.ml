(* 10. Run-length encoding of a list. (easy) *)

(* glutton way *)
let encode_glutton l = List.map (fun x -> (List.length x, List.hd x)) (Problem_9.pack l)

(* optimal way *)
let encode l =
  let rec aux acc ctn = function
    | [] -> []
    | [a] -> (ctn + 1, a) :: acc
    | a :: (b :: _ as tl) ->
       if a = b
       then aux acc (ctn + 1) tl
       else aux ((ctn + 1, a) :: acc) 0 tl
  in
  List.rev (aux [] 0 l)
