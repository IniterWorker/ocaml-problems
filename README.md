# OCAML 99 Problems

## Problems

- [99problems - Ocaml](https://ocaml.org/learn/tutorials/99problems.html)
- [99problems - Progress](PROBLEMS.md)


## Author(s)

- Walter BONETTI <walter.bonetti@epitech.eu>