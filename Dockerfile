FROM ocaml/opam2:alpine-3.8

RUN wget ftp://ftp.gnu.org/gnu/m4/m4-1.4.18.tar.gz \
            && tar -xvzf m4-1.4.18.tar.gz \
            && cd m4-1.4.18 \
            && ./configure --prefix=/usr/local \
            && sudo make install \
            && cd ..  \
            && opam update  \
            && opam install dune ounit -y