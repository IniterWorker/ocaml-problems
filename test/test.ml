open OUnit

open Lib

let get_exn = function
  | Some x -> x
  | None -> assert_failure ("Option.get")

let suite =
  "99Problems" >:::
    [
      "Problem_1" >:::
        [
          "case:1" >:: (fun _ ->
            assert_equal "d" (get_exn (Problem_1.last ["a"; "b"; "c"; "d"]))
          );
          "case:2" >:: (fun _ ->
            assert_equal None (Problem_1.last [])
          );
        ];
      "Problem_2" >:::
        [
          "case:1" >:: (fun _ ->
            assert_equal ("c", "d") (get_exn (Problem_2.last_two ["a"; "b"; "c"; "d"]))
          );
          "case:2" >:: (fun _ ->
            assert_equal None (Problem_2.last_two ["a"])
          );
        ];
      "Problem_3" >:::
        [
          "case:1 at" >:: (fun _ ->
            assert_equal "d" (Problem_3.at 3 ["a"; "b"; "c"; "d"; "e"])
          );
          "case:2 at" >:: (fun _ ->
            try
              ignore (Problem_3.at 3 ["a"]) 
            with
            | Not_found -> ()
            | _ -> assert_failure  "Shound't be different of Not_found"
          );
          "case:1 at_safe" >:: (fun _ ->
            assert_equal "d" (Problem_3.at_safe 3 ["a"; "b"; "c"; "d"; "e"])
          );
          "case:2 at_safe" >:: (fun _ ->
            try
              ignore (Problem_3.at_safe 3 ["a"]) 
            with
            | Not_found -> ()
            | _ -> assert_failure  "Shound't be different of Not_found"
          );
          "case:1 at_safe_option" >:: (fun _ ->
            assert_equal "d" (get_exn (Problem_3.at_safe_option 3 ["a"; "b"; "c"; "d"; "e"]))
          );
          "case:2 at_safe_option" >:: (fun _ ->
            assert_equal None (Problem_3.at_safe_option 3 ["a"])
          );
        ];
      "Problem_4" >:::
        [
          "case:1" >:: (fun _ ->
            assert_equal 3 (Problem_4.length["a"; "b"; "c"])
          );
          "case:2" >:: (fun _ ->
            assert_equal 0 (Problem_4.length [])
          );
        ];
      "Problem_5" >:::
        [
          "case:1" >:: (fun _ ->
            assert_equal ["c"; "b"; "a"] (Problem_5.rev["a"; "b"; "c"])
          );
          "case:2" >:: (fun _ ->
            assert_equal [] (Problem_5.rev [])
          );
        ];
      "Problem_6" >:::
        [
          "case:1" >:: (fun _ ->
            assert_equal true (Problem_6.is_palindrome ["x" ; "a" ; "m" ; "a" ; "x"])
          );
          "case:2" >:: (fun _ ->
            assert_equal true (not (Problem_6.is_palindrome ["a"; "b"]))
          );
        ];
      "Problem_7" >:::
        [
          "case:1" >:: (fun _ ->
            let open Problem_7 in
                 assert_equal ["a"; "b"; "c"; "d"; "e"]
                   (flatten [ One "a" ; Many [ One "b" ; Many [ One "c" ; One "d" ] ; One "e" ] ])
          );
        ];
       "Problem_8" >:::
        [
          "case:1" >:: (fun _ ->
            let res =
              (
                Problem_8.compress
                  ["a";"a";"a";"a";"b";"c";"c";"a";"a";"d";"e";"e";"e";"e"]
              )
            in
            assert_equal ["a";"b";"c";"a";"d";"e"] res 
          );
        ];
       "Problem_9" >:::
         [
           "case:1" >:: (fun _ ->
             let res =
               (
                 Problem_9.pack
                   ["a";"a";"a";"a";"b";"c";"c";"a";"a";"d";"d";"e";"e";"e";"e"]
               )
             in
             assert_equal [["a"; "a"; "a"; "a"]; ["b"]; ["c"; "c"]; ["a"; "a"]; ["d"; "d"]; ["e"; "e"; "e"; "e"]] res 
           );
         ];
       "Problem_9" >:::
         [
           "case:1" >:: (fun _ ->
             let res =
               (
                 Problem_10.encode ["a";"a";"a";"a";"b";"c";"c";"a";"a";"d";"e";"e";"e";"e"]
               )
             in
             assert_equal [(4, "a"); (1, "b"); (2, "c"); (2, "a"); (1, "d"); (4, "e")] res 
           );
            "case:1 glutton" >:: (fun _ ->
             let res =
               (
                 Problem_10.encode_glutton ["a";"a";"a";"a";"b";"c";"c";"a";"a";"d";"e";"e";"e";"e"]
               )
             in
             assert_equal [(4, "a"); (1, "b"); (2, "c"); (2, "a"); (1, "d"); (4, "e")] res 
           );
         ]; 
    ]
  
let _ = run_test_tt_main suite
